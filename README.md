# About

CalmFire is a small toy programming language and its compiler with the following goals:
- Use QBE as a backend
- Very basic type system: integer, pointer, boolean, floating point number
- No need for C library
- Manual memory management
- Work on Linux
- Self hosted (at some far and hypothetical point in the future)
