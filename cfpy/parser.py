from dataclasses import dataclass
from enum import Enum, auto

from .misc import check, CFError, mysplitlines


def parse_prog(source):
    s = State(source)
    yield from _parse_prog(s)
    expect(s, Tk.EOF)


def _parse_prog(state):
    tk = Tk.UNEXPECTED

    while tk not in {Tk.EOF, Tk.CBRA, Tk.CPAR}:
        yield parse_cmd(state)
        tk = state.peek()


def parse_cmd(state):
    tk = state.token()
    if tk.type != Tk.SYMBOL:
        raise CFError(f"Unexpected token {tk} when looking for a trigger", tk)

    kw = keywords.get(tk.text, None)
    if kw:
        params = []
        trig = kw
    else:
        params = [Symbol(tk)]
        trig = Kw.CALL

    while True:
        next_tk = state.peek()
        if next_tk in {Tk.EOF, Tk.CPAR, Tk.CBRA}:
            break
        next_param = parse_param(state)
        if isinstance(next_param, Stop):
            break
        elif isinstance(next_param, FinalBlock):
            params.append(next_param)
            break
        else:
            params.append(next_param)

    return Command(trig, params, tk)


class Tk(Enum):
    UNEXPECTED = -1
    EOF = 0
    OPAR = 1
    CPAR = 2
    OBRA = 3
    CBRA = 4
    SEMI = 5
    COLON = 6
    SYMBOL = 7
    NUMBER = 8
    STRING = 9
    CSTRING = 10


def parse_param(state):
    tk = state.token()
    if tk.type == Tk.SYMBOL:
        return Symbol(tk)
    elif tk.type == Tk.NUMBER:
        return Number(tk)
    elif tk.type in {Tk.CBRA, Tk.CPAR, Tk.SEMI, Tk.EOF}:
        return Stop(tk)
    elif tk.type in {Tk.CSTRING, Tk.STRING}:
        return String(tk)
    elif tk.type == Tk.OBRA:
        bl = FinalBlock(tk, list(_parse_prog(state)))
        expect(state, Tk.CBRA)
        return bl
    elif tk.type == Tk.OPAR:
        bl = Block(tk, list(_parse_prog(state)))
        expect(state, Tk.CPAR)
        return bl
    elif tk.type == Tk.COLON:
        if state.peek() == Tk.OBRA:
            tk = state.token()
            bl = Block(tk, list(_parse_prog(state)))
            expect(state, Tk.CBRA)
            return bl
        else:
            return FinalBlock(tk, [parse_cmd(state)])
    else:
        raise CFError(f"Unexpected token {tk} when looking for a parameter", tk)


def expect(state, token):
    tk = state.token()
    check(tk.type == token, f"Expected {token} but got {tk}.", tk)


@dataclass(frozen=True)
class Token:
    type: Tk
    text: str
    pos: tuple[int, int, int]

    def __repr__(self):
        return f"{self.type}({self.text!r})"

    @classmethod
    def derive(cls, text, other):
        return cls(other.type, text, other.pos)


@dataclass
class Command:
    trig: str
    params: list
    tk: Token


@dataclass(frozen=True)
class Param:
    tk: Token


class Atom(Param):
    @classmethod
    def adhoc(cls, content):
        return cls(Token(cls.tktype, content, (-1, -1, -1)))


class Stop(Param):
    pass


@dataclass(frozen=True)
class Block(Param):
    cmds: list[Command]


class FinalBlock(Block):
    pass


class Nil(Param):
    pass


class Number(Atom):
    tktype = Tk.NUMBER

    @property
    def num(self):
        return int(self.tk.text)

    @classmethod
    def const(cls, n):
        return cls.adhoc(str(n))


class Symbol(Atom):
    tktype = Tk.SYMBOL

    @property
    def name(self):
        return self.tk.text

    def __eq__(self, other):
        return isinstance(other, Symbol) and other.name == self.name

    def __hash__(self):
        return hash(self.name)


class String(Atom):
    tktype = Tk.STRING

    @property
    def value(self):
        if self.cstr:
            txt = self.tk.text[1:]
        else:
            txt = self.tk.text
        return eval(txt).encode("utf8") + b'\0'

    @property
    def cstr(self):
        return self.tk.text.startswith("c")


class Kw(Enum):
    IF = auto()
    ELSE = auto()
    ELIF = auto()
    WHILE = auto()
    LET = auto()
    SET = auto()
    PROC = auto()
    DECL = auto()
    RETURN = auto()
    IMPORT = auto()
    EXPORT = auto()
    CALL = auto()
    TYPE = auto()
    GLOB = auto()


keywords = {
    "if": Kw.IF,
    "else": Kw.ELSE,
    "elif": Kw.ELIF,
    "while": Kw.WHILE,
    "let": Kw.LET,
    "set": Kw.SET,
    "proc": Kw.PROC,
    "decl": Kw.DECL,
    "return": Kw.RETURN,
    "import": Kw.IMPORT,
    "export": Kw.EXPORT,
    "type": Kw.TYPE,
    "glob": Kw.GLOB,
}


single_char_tokens = {
    "(": Tk.OPAR,
    ")": Tk.CPAR,
    "{": Tk.OBRA,
    "}": Tk.CBRA,
    ";": Tk.SEMI,
    ":": Tk.COLON,
}


def isspecial(c):
    return c in single_char_tokens or c in {'"'}


class State:
    def __init__(self, source):
        self.source = source
        self.cursor = 0
        self.match_len = 0
        self.col = 0
        self.line = 1

    def pos(self):
        return (self.cursor, self.col, self.line)

    def token(self):
        tk = self._token()
        txt = self.source[self.cursor : self.cursor + self.match_len]
        return Token(
            tk,
            txt,
            self.pos(),
        )

    def peek(self):
        tk = self._token()
        self.match_len = 0
        return tk

    def _token(self):
        self.cursor += self.match_len
        self.match_len = 0
        try:
            c = self.source[self.cursor]
        except IndexError:
            return Tk.EOF

        while c.isspace():
            self.cursor += 1
            try:
                c = self.source[self.cursor]
            except IndexError:
                return Tk.EOF

        while self.source[self.cursor: self.cursor + 2] == "//":
            while c != "\n":
                self.cursor += 1
                try:
                    c = self.source[self.cursor]
                except IndexError:
                    return Tk.EOF

            while c.isspace():
                self.cursor += 1
                try:
                    c = self.source[self.cursor]
                except IndexError:
                    return Tk.EOF

        passed = mysplitlines(self.source[: self.cursor])
        self.line = len(passed)
        if self.line == 0:
            self.line = 1
            self.col = 0
        else:
            self.col = len(passed[-1]) + 1

        if c in single_char_tokens:
            self.match_len = 1
            return single_char_tokens[c]
        elif self.source[self.cursor: self.cursor+2] == 'c"':
            self.match_len = 1
            while c != '"':
                self.match_len += 1
                try:
                    c = self.source[self.cursor + self.match_len]
                    if c == "\\":
                        self.match_len += 1
                except IndexError:
                    return Tk.EOF
            self.match_len += 1
            return Tk.CSTRING
        elif c == '"':
            first = True
            while first or c != '"':
                self.match_len += 1
                first = False
                try:
                    c = self.source[self.cursor + self.match_len]
                    if c == "\\":
                        self.match_len += 1
                except IndexError:
                    return Tk.EOF
            self.match_len += 1
            return Tk.STRING

        number = True

        first = True
        while not c.isspace() and not isspecial(c):
            self.match_len += 1
            try:
                number = number and (c.isdigit() or (first and c == "-"))
                c = self.source[self.cursor + self.match_len]
            except IndexError:
                break
            first = False

        if self.match_len == 1 and self.source[self.cursor] == "-":
            return Tk.SYMBOL

        if number:
            return Tk.NUMBER
        else:
            return Tk.SYMBOL
