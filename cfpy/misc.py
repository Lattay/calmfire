import os.path

from collections.abc import Mapping
from collections import OrderedDict


install_dir = os.path.dirname(os.path.dirname(__file__))
std_dir = os.path.join(install_dir, "std")


class CFError(Exception):
    def __init__(self, msg, tk=None):
        super().__init__(msg)
        self.tk = tk


def check(test, msg, tk=None):
    if not test:
        raise CFError(f"Failed check: {msg}", tk)


def pairs(it):
    prev = None
    for val in it:
        if prev:
            yield (prev, val)
            prev = None
        else:
            prev = val
    if prev:
        raise ValueError("Unpaired elements")


def mysplitlines(text):
    return text.split("\n")


class FrozenDict(Mapping):
    def __init__(self, fields):
        self.data = OrderedDict(fields)

    def __getitem__(self, key):
        return self.data[key]

    def __hash__(self):
        return hash(tuple(self.data.items()))

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        yield from self.data

    def __eq__(self, other):
        return isinstance(other, FrozenDict) and all(
            ka == kb and va == vb
            for (ka, va), (kb, vb) in zip(self.data.items(), other.data.items())
        )


def normalize(name):
    name = (
        name.replace(".", "_")
        .replace("->", "_to_")
        .replace("-", "_")
        .replace("=", "eq")
    )
    if name.endswith("?"):
        name = "is_" + name[:-1]

    chars = []

    for c in name:
        if c not in word_chars:
            chars.append(f"_{ord(c)}_")
        else:
            chars.append(c)

    name = "".join(chars)

    chars.clear()
    acc = 0
    for c in name:
        if c == "_":
            acc += 1
            continue

        if acc > 0:
            chars.append("_")
            acc = 0

        chars.append(c)

    if acc > 0:
        chars.append("_")

    if chars[0][0].isdecimal():
        return "_" + "".join(chars)
    else:
        return "".join(chars)


word_chars = set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
