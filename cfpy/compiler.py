import os.path
import sys
from typing import Optional

from contextlib import contextmanager
from enum import Enum

from .misc import CFError, check, pairs, FrozenDict, std_dir, install_dir, normalize
from .parser import (
    Block,
    Command,
    FinalBlock,
    Kw,
    Nil,
    Number,
    Param,
    parse_prog,
    String,
    Symbol,
    Token,
)
from .analyser import seam_ifs, topbottom, collect_proc_sig, collect_exported, collect_types
from .type import (
    AtomType,
    builtins_types,
    check_cast_compat,
    convert,
    DistinctType,
    free_cast,
    isinttype,
    isniltype,
    istype,
    op_cast,
    RecordType,
    ssa_type,
    ssa_first_class_type,
    Type,
    int_types,
    String as StringRec,
    resolve_int,
)

search_paths = [std_dir, os.path.join(install_dir, "cfc")]


test_core = os.path.join(install_dir, "tests/no_libc_no_std", "no_std_rt.ssa")


def src_to_ssa(src, target, test_mode=False):

    code, _ = compile_file(src, CrossModuleState(), test_mode)
    if code is None:
        return False

    with open(target, "w") as f:
        if test_mode:
            with open(test_core, "r") as fc:
                f.write(fc.read())
        for snips in code:
            f.write(snips)

    return True


class CrossModuleState:
    def __init__(self):
        self.imported = set()
        self.c = 0

    def count(self):
        self.c += 1
        return self.c


def compile_file(src, xmodstate, test_mode):
    source = open(src, "r").read()

    try:
        cmds = list(parse_prog(source))
    except CFError as e:
        report(src, source, e)
        return None, None

    if test_mode:
        state = CompileState(cf_test_builtins)
    else:
        state = CompileState(cf_builtins)

    state.xmod = xmodstate

    code_snips = []

    try:
        imported = list(extract_imported(cmds))
    except CFError as e:
        report(src, source, e)
        return None, None

    for im_src, pref in imported:
        code, mod_state = compile_file(im_src, xmodstate, test_mode)
        if code is None:
            return None, None
        state.register_mod(pref, mod_state)

        if im_src not in xmodstate.imported:
            code_snips.extend(code)

        xmodstate.imported.add(im_src)

    try:
        code_snips.extend(compile_(cmds, state))
    except CFError as e:
        report(src, source, e)
        return None, None

    return code_snips, state


def report(path, source, e):
    _, col, lin = e.tk.pos
    print(f"{path}:{lin}:{col}", file=sys.stderr)
    print(str(e), file=sys.stderr)

    for i, line in enumerate(source.splitlines()):
        if lin - 2 <= i + 1 <= lin + 2:
            if i + 1 == lin:
                pad = " " * col
                print(f">{i+1:5} {line}", file=sys.stderr)
                print(f"      {pad}^", file=sys.stderr)
            else:
                print(f" {i+1:5} {line}", file=sys.stderr)


def extract_imported(cmds):
    for cmd in cmds:
        if cmd.trig == Kw.IMPORT:
            check(
                len(cmd.params) == 2,
                "Import takes two parameters: a path and a symbol",
                cmd.tk,
            )
            p, s = cmd.params
            check(
                isinstance(p, Symbol),
                "First import parameter must be a symbol.",
                cmd.tk,
            )
            check(
                isinstance(s, Symbol),
                "Second import parameter must be a symbol.",
                cmd.tk,
            )
            for d in search_paths:
                src_path = os.path.join(d, p.name + ".cf")
                if os.path.exists(src_path):
                    yield (src_path, s)
                    break
            else:
                raise CFError(f"No module found for path {p.name}", cmd.tk)


comp_ops = {
    "<": ("slt", "ult", "lt"),
    ">": ("sgt", "ugt", "gt"),
    "<=": ("sle", "ule", "le"),
    ">=": ("sge", "uge", "ge"),
    "=": ("eq", "eq", "eq"),
    "!=": ("ne", "ne", "ne"),
}


cf_builtins = {
    "cf_memcpy": {
        "returns": AtomType("nil"),
        "params": [AtomType("ptr"), AtomType("ptr"), AtomType("int")],
    },
    "syscall1": {
        "returns": AtomType("int"),
        "params": [AtomType("int"), AtomType("int")],
    },
    "syscall2": {
        "returns": AtomType("int"),
        "params": [AtomType("int"), AtomType("int"), AtomType("int")],
    },
    "syscall3": {
        "returns": AtomType("int"),
        "params": [AtomType("int"), AtomType("int"), AtomType("int"), AtomType("int")],
    },
    "syscall4": {
        "returns": AtomType("int"),
        "params": [
            AtomType("int"),
            AtomType("int"),
            AtomType("int"),
            AtomType("int"),
            AtomType("int"),
        ],
    },
}

cf_test_builtins = cf_builtins.copy()
cf_test_builtins.update(
    {
        "putu": {
            "returns": AtomType("i32"),
            "params": [AtomType("int")],
        },
        "write": {
            "returns": AtomType("i32"),
            "params": [AtomType("int"), AtomType("ptr"), AtomType("int")],
        },
    }
)


class SymType(Enum):
    LOCAL = 0
    GLOBAL = 1
    LABEL = 2
    TYPE = 3


class CompileState:
    def __init__(self, builtins_proc):
        self.scope = [{}]
        self.stack = []
        self.c = 0
        self.running_block = False
        self.procs = builtins_proc.copy()
        self.data = {}
        self.types = {t: AtomType(t) for t in builtins_types}
        self.named_types = {}
        self.modules = {}
        self.exported = {"main"}
        self.xmod = None

    def gensym(self, type_=SymType.LOCAL, name=""):
        assert isinstance(type_, SymType)
        c = self.xmod.count()

        if name:
            name_ = "_" + normalize(name)
        else:
            name_ = ""

        if type_ == SymType.LOCAL:
            return f"%v{c}{name_}"
        elif type_ == SymType.LABEL:
            return f"@l{c}{name_}"
        elif type_ == SymType.TYPE:
            return f":t{c}{name_}"
        else:
            return f"$v{c}{name_}"

    def register_data(self, symbol, data, tk=None):
        if tk is None:
            assert symbol not in self.data
        check(symbol not in self.data, f"There is already a global piece of data named {symbol}.", tk)
        self.data[symbol] = data

    def register_mod(self, prefix, state):
        self.modules[prefix.name] = state

    def bind(self, name, type_, var):
        assert istype(type_), f"Unexpected type {type_}"
        assert isinstance(var, str), f"Unexpected {var}"
        assert isinstance(name, Symbol), f"Unexpected {name}"
        if name.name in self.scope[-1]:
            raise CFError(f"Symbol {name.name} already exists in current scope.",
                          name.tk)
        self.scope[-1][name.name] = (var, compile_type_expr(type_, self))

    def lookup_t(self, symbol):
        assert isinstance(symbol, Symbol), f"Unexpected {symbol}"
        for frame in reversed(self.scope):
            if symbol.name in frame:
                return frame[symbol.name]

        if "." in symbol.name:
            m, *rest = symbol.name.split(".")
            if m in self.modules:
                unqual = Symbol(Token.derive(".".join(rest), symbol.tk))
                try:
                    return self.modules[m].lookup_t(unqual)
                except CFError:
                    pass

        raise CFError(f"Unbound symbol {symbol.name}.", symbol.tk)

    def lookup_type(self, t):
        assert not isinstance(t, str)
        assert istype(t) or isinstance(t, Symbol), f"Unexpected type {t}"
        if isinstance(t, Symbol):
            try:
                return self.types[t.name]
            except KeyError:
                pass

            for mod in self.modules.values():
                try:
                    return mod.lookup_type(t)
                except CFError:
                    pass
            raise CFError(f"Unbound type {t.name}", t.tk)
        else:
            return t

    def lookup_proc(self, sym):
        assert isinstance(sym, Symbol)
        try:
            proc = self.procs[sym.name]
            proc["returns"] = compile_type_expr(proc["returns"], self)
            proc["params"] = [compile_type_expr(t, self) for t in proc["params"]]
            proc["func_name"] = sym.name
            return proc
        except KeyError:
            pass

        try:
            type_ = self.lookup_type(sym)
            return {
                "returns": type_,
                "params": [None],
                "cast": True,
            }
        except CFError:
            pass

        pref, *rest = sym.name.split(".")

        s = Symbol(Token.derive(pref, sym.tk))
        try:
            base = self.lookup_type(s)
        except CFError:
            pass
        else:
            check(
                isinstance(base, RecordType),
                "Only record have accessible fields.",
                sym.tk,
            )
            if len(rest) > 1:
                raise NotImplementedError("Nested records are not yet supported.")
            else:
                field = rest[0]
                check(field in base.fields, f"No {field} field in this type.", sym.tk)
                return {
                    "field_op": True,
                    "type": base,
                    "offset": base.get_offset(field),
                    "field": base.fields[field],
                    "name": field,
                    "func_name": sym.name,
                }

        if pref in self.modules:
            unqual = Symbol(Token.derive(".".join(rest), sym.tk))
            try:
                return self.modules[pref].lookup_proc(unqual)
            except CFError:
                pass

        raise CFError(f"Unbound procedure {sym.name}.", sym.tk)

    def pop_t(self, n=1):
        if n == 0:
            return []
        new = self.stack[:-n]
        poped = self.stack[-n:]
        self.stack = new
        assert len(poped) == n, f"Length is {len(poped)} != {n}"
        return poped

    def pop(self, n=1):
        poped = self.pop_t(n=n)
        return [v for (v, t) in poped]

    def push_t(self, expr, type_):
        assert isinstance(expr, (int, Number, str, Nil)), f"Unexpected value {expr}"
        assert type_ is None or istype(type_), f"Unexpected type {type_}"
        if isinstance(expr, Number):
            self.stack.append((expr.num, type_))
        else:
            self.stack.append((expr, type_))

    def push(self, expr):
        self.push_t(expr, None)

    @contextmanager
    def new_scope(self):
        self.scope.append({})
        yield
        self.scope.pop()


def compile_(cmds, state):
    cmds = list(seam_ifs(cmds))
    # validate that top level things and not-toplevel things are well
    # placed
    topbottom(cmds)

    collect_types(
        state.types,
        cmds,
        istype,
        lambda t: compile_type_expr(t, state),
    )

    collect_exported(state.exported, cmds)

    collect_proc_sig(
        state.procs,
        cmds,
        istype,
    )

    def gen_code():
        for cmd in cmds:
            yield from compile_cmd(cmd, state)

    def gen_data():
        for name, value in state.data.items():
            if isinstance(value, bytes):
                yield f"data {name} = {{ b"
                for v in value:
                    yield " " + str(v)
                yield " }\n"

            elif isinstance(value, int):
                yield f"data {name} = {{ z {value} }}\n"

            elif isinstance(value, Constant):
                if value.type_ == AtomType("ptr"):
                    yield f"data {name} = {{ b "
                    assert isinstance(value.value, bytes)
                    yield " ".join(str(v) for v in value.value)
                    yield " }\n"
                elif isinstance(value.type_, RecordType):
                    raise NotImplementedError("Record globals not yet implemented.")
                else:
                    yield f"data {name} = {{ {ssa_type(value.type_)} {value.value} }}\n"

            else:
                raise NotImplementedError(f"Data {value} is not implemented yet.")

    code = list(gen_code())
    yield from gen_data()
    yield from code


def compile_cmd(cmd: Command, state: CompileState):
    n = len(state.stack)
    assert isinstance(cmd, Command), f"Unexpected {cmd}"
    if cmd.trig == Kw.LET:
        check(
            len(cmd.params) in {2, 3},
            "let takes two or three parameters: a symbol, a type and optionally a value.",
            cmd.tk,
        )
        s, t, *rest = cmd.params
        check(isinstance(s, Symbol), "let applies to symbols.", s.tk)
        type_ = compile_type_expr(t, state)
        var = state.gensym(name=s.name)
        check(not isniltype(type_), "The nil type cannot be used in bindings.", t.tk)
        state.bind(s, type_, var)

        if rest:
            val = rest[0]
            yield from compile_param(val, state)

            [(cval, tval)] = state.pop_t()

            check_type(type_, tval, val.tk)

            yield from emit_bind(type_.resolve(), var, cval)
        else:
            sz = type_.size()
            yield from emit_alloc(var, sz)

        state.push(var)

    elif cmd.trig == Kw.SET:
        check(
            len(cmd.params) == 2,
            "set takes two parameters: a symbol and a value.",
            cmd.tk,
        )
        s, val = cmd.params
        check(isinstance(s, Symbol), "set applies to symbols.", s.tk)
        yield from compile_param(val, state)
        [(cval, tval)] = state.pop_t()

        var, t = state.lookup_t(s)

        check_type(t, tval, val.tk)

        t = t.resolve()

        if isinstance(t, RecordType):
            # TODO Add a test to check that this code path works even if var is a global
            yield from emit_nil_call(
                "cf_memcpy", [(cval, t), (var, t), (tval.size(), AtomType("int"))]
            )

        else:
            if isglobal(var):
                # TODO
                # If var points to a long or smaller, set the memory region
                # If var points to something that is a pointer anyway I thing that
                # it already work
                raise NotImplementedError("Globals are not implemented yet")
            yield from emit_bind(t.resolve(), var, cval)


        state.push(var)
    elif cmd.trig == Kw.PROC:
        sym, retype, *params = cmd.params[:-1]
        body = cmd.params[-1]

        if len(params) % 2 != 0:
            raise CFError(
                "There is an untyped parameter or an unexpected type.", params[0].tk
            )
        typed_params = list(pairs(params))

        crettype = compile_type_expr(retype, state)
        rcrettype = crettype.resolve()

        name = sym.name

        with state.new_scope():
            norm_name = normalize(name)

            type_ = ssa_first_class_type(crettype)

            if norm_name in state.exported:
                yield "export\n"

            if isinstance(rcrettype, RecordType):
                yield f"function ${norm_name} ("
                outparam = state.gensym(name="out")
                state.bind(Symbol.adhoc("%out"), crettype, outparam)
                yield f"l {outparam}, "
            else:
                yield f"function {type_} ${norm_name} ("
                state.bind(Symbol.adhoc("%out"), crettype, "")

            for param, ptype in typed_params:
                assert isinstance(param, Symbol)
                var = state.gensym(name=param.name)
                type_ = compile_type_expr(ptype, state)
                ctype = ssa_first_class_type(type_)
                state.bind(param, type_, var)
                yield f"{ctype} {var}, "

            yield ") {\n"
            yield from emit_label(state, "@start")

            yield from compile_param(body, state)

            if state.running_block:
                state.pop_t()
                v, _ = state.lookup_t(Symbol.adhoc("%out"))
                # TODO raise an error if %out has never been used.
                if isinstance(rcrettype, RecordType):
                    yield from emit_return(state)
                else:
                    yield from emit_return(state, v)
                state.push_t(v, crettype)

            assert not state.running_block

            yield "}\n"

    elif cmd.trig == Kw.DECL:
        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.GLOB:
        check(
            len(cmd.params) in {2, 3},
            "glob takes two or three parameters: a symbol, a type and optionally a value.",
            cmd.tk,
        )
        s, t, *rest = cmd.params
        check(isinstance(s, Symbol), "glob applies to symbols.", s.tk)
        type_ = compile_type_expr(t, state)

        check(not isniltype(type_), "The nil type cannot be used in bindings.", t.tk)

        var = "$" + normalize(s.name)

        state.bind(s, type_, var)

        if rest:
            val = rest[0]
            if not (isinstance(val, Symbol) and val.name == "extern"):
                val = rest[0]

                const = eval_const(val, state)

                state.register_data(var, const, tk=s.tk)
        else:
            sz = type_.size()
            state.register_data(var, sz, tk=s.tk)

        state.push(var)

    elif cmd.trig == Kw.EXPORT:
        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.RETURN:
        check(
            len(cmd.params) <= 1,
            "Can only return one or zero value.",
            cmd.tk,
        )

        v, t = state.lookup_t(Symbol.adhoc("%out"))

        if len(cmd.params) == 1:
            yield from compile_param(cmd.params[0], state)

            [(cval, tval)] = state.pop_t()

            check_type(t, tval, cmd.params[0].tk)

            tval = tval.resolve()

            if v:
                yield from emit_nil_call(
                    "cf_memcpy", [(cval, tval), (v, tval), (tval.size(), AtomType("int"))]
                )
                yield from emit_return(state)
            else:
                yield from emit_return(state, cval)
        else:
            if isniltype(t) or isinstance(t, RecordType):
                yield from emit_return(state)
            else:
                # TODO raise an error if %out has never been used
                yield from emit_return(state, v)

        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.IF:
        assert len(cmd.params) >= 2, f"Buggy if {cmd}"
        pr = cmd.params.copy()
        pr.reverse()
        finallbl = state.gensym(SymType.LABEL)

        can_get_out = False

        while len(pr) >= 2:
            cond = pr.pop()
            body = pr.pop()
            var = state.gensym()
            iflbl = state.gensym(SymType.LABEL)
            elselbl = state.gensym(SymType.LABEL)
            assert state.running_block
            yield from compile_param(cond, state)

            [(cval, tval)] = state.pop_t()

            check_type(AtomType("u8"), tval, cond.tk)

            yield from emit_test(state, iflbl, elselbl, cval)
            yield from emit_label(state, iflbl)
            yield from compile_param(body, state)
            state.pop()
            if state.running_block:
                can_get_out = True
                yield from emit_jmp(state, finallbl)
            yield from emit_label(state, elselbl)

        if len(pr) == 1:
            body = pr.pop()
            yield from compile_param(body, state)
            state.pop()

        if state.running_block:
            can_get_out = True
            yield from emit_jmp(state, finallbl)

        if can_get_out:
            yield from emit_label(state, finallbl)
        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.WHILE:
        # TODO An infinite loop was to break only through return it would still
        # need a dummy return after it because the control flow is not analysed
        # properly
        check(len(cmd.params) == 2, "A while must have a condition and a body.", cmd.tk)
        cond, body = cmd.params

        testlbl = state.gensym(SymType.LABEL)
        startloop = state.gensym(SymType.LABEL)
        endloop = state.gensym(SymType.LABEL)

        yield from emit_jmp(state, testlbl)
        yield from emit_label(state, testlbl)
        yield from compile_param(cond, state)

        [(cval, tval)] = state.pop_t()

        check_type(AtomType("u8"), tval, cond.tk)

        yield from emit_test(state, startloop, endloop, cval)
        yield from emit_label(state, startloop)
        yield from compile_param(body, state)
        state.pop()
        yield from emit_jmp(state, testlbl)
        yield from emit_label(state, endloop)
        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.TYPE:
        state.push_t(Nil(cmd.tk), AtomType("nil"))

    elif cmd.trig == Kw.CALL:
        assert len(cmd.params) >= 1
        func, *params = cmd.params

        if func.name == "+":
            for p in params:
                yield from compile_param(p, state)

            exprs = state.pop_t(len(params))

            t = reduce_int_types(params, exprs)

            def emit_add(var: str, a: str | int, b: str | int):
                return emit_binop("add", t, var, a, b)

            yield from binary_reduce(emit_add, Number.const(0), state, unzip0(exprs))
            res, *_ = state.pop()
            # TODO pick the best int type
            state.push_t(res, t)

        elif func.name == "-":
            s = state.gensym()

            for p in params:
                yield from compile_param(p, state)

            exprs = state.pop_t(len(params))

            t = reduce_int_types(params, exprs)

            if len(params) == 1:
                (v, _) = exprs[0]

                yield from emit_unop("neg", s, v)
            else:
                (cfirst, _), *rest = exprs

                def emit_add(var: str, a: str | int, b: str | int):
                    return emit_binop("add", t, var, a, b)

                yield from binary_reduce(emit_add, Number.const(0), state, unzip0(rest))
                yield from emit_binop("sub", t, s, cfirst, *state.pop())
            state.push_t(s, t)

        elif func.name == "*":
            for p in params:
                yield from compile_param(p, state)

            exprs = state.pop_t(len(params))

            t = reduce_int_types(params, exprs)

            def emit_mul(var: str, a: str | int, b: str | int):
                return emit_binop("mul", t, var, a, b)

            yield from binary_reduce(emit_mul, Number.const(1), state, unzip0(exprs))
            res, *_ = state.pop()
            state.push_t(res, t)

        elif func.name == "/":
            s = state.gensym()

            for p in params:
                yield from compile_param(p, state)

            exprs = state.pop_t(len(params))

            t = reduce_int_types(params, exprs)

            if len(params) == 1:
                raise NotImplementedError(
                    "Invert does not make sense when you don't have float."
                )
                yield from emit_unop("neg", s, *state.pop())
            else:
                (cfirst, _), *rest = exprs

                def emit_mul(var: str, a: str | int, b: str | int):
                    return emit_binop("mul", t, var, a, b)

                yield from binary_reduce(emit_mul, Number.const(0), state, unzip0(rest))
                yield from emit_binop("div", t, s, cfirst, *state.pop())
            state.push_t(s, t)

        elif func.name == "&+":
            check(len(params) >= 2, "&+ takes at least two parameters", cmd.tk)
            for p in params:
                yield from compile_param(p, state)

            (cfirst, tfirst), *rest = state.pop_t(len(params))

            check_type(AtomType("ptr"), tfirst, params[0].tk)

            for p, (_, t) in zip(params[1:], rest):
                check_type(AtomType("int"), t, p.tk)

            def emit_add(var: str, a: str | int, b: str | int):
                return emit_binop("add", AtomType("int"), var, a, b)

            yield from binary_reduce(emit_add, Number.const(0), state, unzip0(rest))
            s = state.gensym()

            yield from emit_binop("add", AtomType("ptr"), s, cfirst, *state.pop())
            state.push_t(s, AtomType("ptr"))

        elif func.name == "&-":
            check(len(params) >= 2, "&- takes at least two parameters", cmd.tk)
            for p in params:
                yield from compile_param(p, state)

            (cfirst, tfirst), *rest = state.pop_t(len(params))

            check_type(AtomType("ptr"), tfirst, params[0].tk)

            for p, (_, t) in zip(params[1:], rest):
                check_type(AtomType("int"), t, p.tk)

            def emit_add(var: str, a: str | int, b: str | int):
                return emit_binop("add", AtomType("int"), var, a, b)

            yield from binary_reduce(emit_add, Number.const(0), state, unzip0(rest))
            s = state.gensym()
            yield from emit_binop("sub", AtomType("ptr"), s, cfirst, *state.pop())
            state.push_t(s, AtomType("ptr"))

        elif func.name == "mod":
            check(len(params) == 2, "mod expect exactly two parameters.", cmd.tk)
            a, b = params
            yield from compile_param(a, state)
            yield from compile_param(b, state)

            [(ca, ta), (cb, tb)] = state.pop_t(2)

            t = reduce_int_types([a, b], [(ca, ta), (cb, tb)])

            var = state.gensym()
            yield from emit_binop("rem", t, var, ca, cb)
            state.push_t(var, t)

        elif func.name in comp_ops:
            check(
                len(params) >= 2,
                "Comparisions applies to two or more parameters",
                cmd.tk,
            )
            if len(params) == 2:
                a, b = params
                yield from compile_param(a, state)
                yield from compile_param(b, state)
                [(ca, ta), (cb, tb)] = state.pop_t(2)

                check(isinttype(ta.resolve()), "Comparisions only applies to integers", a.tk)
                check(isinttype(tb.resolve()), "Comparisions only applies to integers", b.tk)
                check_type(ta, tb, b.tk)
                t = resolve_int(ta.resolve(), tb.resolve())

                v = state.gensym()
                yield from emit_cmp(t, comp_ops[func.name], v, ca, cb)
                state.push_t(v, AtomType("u8"))
            else:
                raise NotImplementedError(
                    "< with more than two operands is not implemented yet."
                )

        elif func.name == "&local":
            check(len(params) == 1, "&local takes exactly one parameter.", cmd.tk)
            sz = params[0]
            yield from compile_param(sz, state)
            var = state.gensym()
            [(cval, tval)] = state.pop_t()
            check_type(AtomType("int"), tval, sz.tk)
            yield from emit_alloc(var, cval)
            state.push_t(var, AtomType("ptr"))

        elif func.name == "!1":
            check(len(params) == 1, "!1 get exactly one parameter.", cmd.tk)
            target = params[0]
            yield from compile_param(target, state)
            [(ctarget, ttarget)] = state.pop_t()
            check_type(AtomType("ptr"), ttarget, target.tk)
            s = state.gensym()
            yield from emit_load(AtomType("u8"), s, ctarget)
            state.push_t(s, AtomType("u8"))

        elif func.name == "set!1":
            check(len(params) == 2, "set!1 get exactly two parameters.", cmd.tk)
            target, value = params
            yield from compile_param(target, state)
            yield from compile_param(value, state)
            [(ctarget, ttarget), (cval, tval)] = state.pop_t(2)
            check_type(AtomType("ptr"), ttarget, target.tk)
            check_type(AtomType("u8"), tval, value.tk)
            yield from emit_store(AtomType("u8"), ctarget, cval)
            state.push_t(cval, tval)

        elif func.name == "get!1":
            check(len(params) == 1, "get!1 get exactly one parameter.", cmd.tk)
            src = params[0]
            yield from compile_param(src, state)
            [(csrc, tsrc)] = state.pop_t()

            target = state.gensym()

            check_type(AtomType("ptr"), tsrc, src.tk)
            yield from emit_load(AtomType("u8"), target, csrc)
            state.push_t(target, AtomType("u8"))

        elif func.name == "0?":
            check(len(params) == 1, "0? expect exactly one parameter.", cmd.tk)
            v = params[0]
            yield from compile_param(v, state)
            var = state.gensym()
            [(cval, tval)] = state.pop_t()
            check(isinttype(tval.resolve()), "0? applies to integer only.", v.tk)

            t = resolve_int(tval.resolve())

            yield from emit_cmp(t, comp_ops["="], var, 0, cval)
            state.push_t(var, AtomType("u8"))

        elif func.name == "&0?":
            check(len(params) == 1, "&0? expect exactly one parameter.", cmd.tk)
            v = params[0]
            yield from compile_param(v, state)
            var = state.gensym()
            [(cval, tval)] = state.pop_t()
            check(tval.resolve() == AtomType("ptr") or isinstance(tval.resolve(), RecordType), "&0? applies to integer only.", v.tk)

            yield from emit_cmp(AtomType("int"), comp_ops["="], var, 0, cval)
            state.push_t(var, AtomType("u8"))

        elif func.name == "1?":
            check(len(params) == 1, "1? expect exactly one parameter.", cmd.tk)
            v = params[0]
            yield from compile_param(v, state)
            var = state.gensym()
            [(cval, tval)] = state.pop_t()
            check(isinttype(tval.resolve()), "1? applies to integer only.", v.tk)

            t = resolve_int(tval.resolve())

            yield from emit_cmp(t, comp_ops["="], var, 1, cval)
            state.push_t(var, AtomType("u8"))

        elif func.name == "+!":
            check(len(params) == 1, "+! expect exactly one parameter.", cmd.tk)
            check(
                isinstance(params[0], Symbol),
                "+! applies only to variables.",
                params[0].tk,
            )

            v = params[0]
            var, t = state.lookup_t(v)

            t = t.resolve()

            check(isinttype(t), "+! applies only to integers", v.tk)

            if isglobal(var):
                s = state.gensym()
                yield from emit_load(t, s, var)
                yield from emit_binop("add", t, s, s, 1)
                yield from emit_store(t, var, s)
            else:
                yield from emit_binop("add", t, var, var, 1)

            state.push_t(var, AtomType("ptr"))

        elif func.name == "-!":
            check(len(params) == 1, "-! expect exactly one parameter.", cmd.tk)
            check(
                isinstance(params[0], Symbol),
                "-! applies only to variables.",
                params[0].tk,
            )

            v = params[0]
            var, t = state.lookup_t(v)

            check(isinttype(t), "+! applies only to integers", v.tk)

            t = t.resolve()

            if isglobal(var):
                s = state.gensym()
                yield from emit_load(t, s, var)
                yield from emit_binop("sub", t, s, s, 1)
                yield from emit_store(t, var, s)
            else:
                yield from emit_binop("sub", t, var, var, 1)

            state.push_t(var, AtomType("ptr"))

        elif func.name == "&+!":
            check(len(params) == 1, "&+! expect exactly one parameter.", cmd.tk)
            check(
                isinstance(params[0], Symbol),
                "&+! applies only to variables.",
                params[0].tk,
            )

            v = params[0]
            var, t = state.lookup_t(v)

            check_type(AtomType("ptr"), t, v.tk)

            t = t.resolve()

            if isglobal(var):
                s = state.gensym()
                yield from emit_load(t, s, var)
                yield from emit_binop("add", t, s, s, 1)
                yield from emit_store(t, var, s)
            else:
                yield from emit_binop("add", t, var, var, 1)

            state.push_t(var, AtomType("ptr"))

        elif func.name == "&-!":
            check(len(params) == 1, "&-! expect exactly one parameter.", cmd.tk)
            check(
                isinstance(params[0], Symbol),
                "&-! applies only to variables.",
                params[0].tk,
            )

            v = params[0]
            var, t = state.lookup_t(v)

            check_type(AtomType("ptr"), t, v.tk)

            t = t.resolve()

            if isglobal(var):
                s = state.gensym()
                yield from emit_load(t, s, var)
                yield from emit_binop("sub", t, s, s, 1)
                yield from emit_store(t, var, s)
            else:
                yield from emit_binop("sub", t, var, var, 1)

            state.push_t(var, AtomType("ptr"))

        elif func.name == "sizeof":
            check(len(params) == 1, "sizeof only applies to one parameter.", func.tk)
            field = params[0]
            check(isinstance(field, Symbol), "offsetof only applies to symbols.", field.tk)

            found = False

            try:
                _, t = state.lookup_t(field)
                found = True
            except CFError:
                pass

            if found:
                state.push_t(t.size(), AtomType("uint"))
            else:
                try:
                    t = state.lookup_type(field)
                    found = True
                except CFError:
                    pass

            if found:
                state.push_t(t.size(), AtomType("uint"))
            else:
                sig = state.lookup_proc(field)
                if sig.get("field_op", False):
                    state.push_t(sig["field"].size(), AtomType("uint"))
                    found = True

            if not found:
                raise CFError("Unbound symbol", field.tk)

        elif func.name == "offsetof":
            check(len(params) == 1, "offsetof only applies to one parameter.", func.tk)
            field = params[0]
            check(isinstance(field, Symbol), "offsetof only applies to symbols.", field.tk)

            sig = state.lookup_proc(field)

            check(sig.get("field_op", False), "offsetof only applies to record field", func.tk)

            state.push_t(sig["offset"], AtomType("uint"))

        elif func.name == "and":
            finallbl = state.gensym(SymType.LABEL)

            pr = params.copy()
            pr.reverse()

            res_sym = state.gensym()
            yield from emit_bind(AtomType("u8"), res_sym, 1)

            while pr:
                cond = pr.pop()

                var = state.gensym()
                iflbl = state.gensym(SymType.LABEL)

                assert state.running_block
                yield from compile_param(cond, state)

                [(cval, tval)] = state.pop_t()

                check_type(AtomType("u8"), tval, cond.tk)

                yield from emit_bind(AtomType("u8"), res_sym, cval)
                yield from emit_test(state, iflbl, finallbl, cval)
                yield from emit_label(state, iflbl)

            assert state.running_block
            yield from emit_label(state, finallbl)
            state.push_t(res_sym, AtomType("u8"))

        elif func.name == "or":
            finallbl = state.gensym(SymType.LABEL)

            pr = params.copy()
            pr.reverse()

            res_sym = state.gensym()
            yield from emit_bind(AtomType("u8"), res_sym, 0)

            while pr:
                cond = pr.pop()

                var = state.gensym()
                iflbl = state.gensym(SymType.LABEL)

                assert state.running_block
                yield from compile_param(cond, state)

                [(cval, tval)] = state.pop_t()

                check_type(AtomType("u8"), tval, cond.tk)

                yield from emit_bind(AtomType("u8"), res_sym, cval)
                yield from emit_test(state, finallbl, iflbl, cval)
                yield from emit_label(state, iflbl)

            assert state.running_block
            yield from emit_label(state, finallbl)
            state.push_t(res_sym, AtomType("u8"))

        else:  # regular call
            sig = state.lookup_proc(func)

            if sig.get("field_op", False):
                check(
                    len(params) in {1, 2},
                    "Field accessor can take only one or two parameters",
                    func.tk,
                )
                if len(params) == 1:
                    # Load field
                    p = params[0]
                    yield from compile_param(p, state)
                    field_ptr = state.gensym(name=sig["name"]+"_p")
                    [(ptr, tptr)] = state.pop_t()
                    check_type(sig["type"], tptr, p.tk)
                    yield from emit_binop("add", AtomType("ptr"), field_ptr, ptr, sig["offset"])
                    sz = sig["field"].size()

                    if isinstance(sig["field"], RecordType):
                        # Just push the pointer
                        state.push_t(field_ptr, sig["field"])
                    else:
                        field_val = state.gensym(name=sig["name"])
                        yield from emit_load(sig["field"].resolve(), field_val, field_ptr)

                        state.push_t(field_val, sig["field"])
                else:
                    # Store field
                    p, val = params
                    yield from compile_param(p, state)
                    yield from compile_param(val, state)
                    field_ptr = state.gensym(name=sig["name"]+"_p")
                    [(ptr, tptr), (cval, ct)] = state.pop_t(2)
                    check_type(sig["field"], ct, val.tk)
                    check_type(sig["type"], tptr, p.tk)
                    yield from emit_binop("add", AtomType("ptr"), field_ptr, ptr, sig["offset"])
                    # if sig["name"] == "len":
                    #     print(sig)
                    yield from emit_store(sig["field"].resolve(), field_ptr, cval)
                    state.push_t(cval, sig["field"])
            elif sig.get("cast", False):
                check(len(params) == 1, "A cast takes exactly one parameter.", func.tk)
                ttarget = sig["returns"]

                yield from compile_param(params[0], state)

                [(v, t)] = state.pop_t()

                if free_cast(t, ttarget):
                    state.push_t(v, ttarget)
                elif op_cast(t, ttarget):
                    op = convert(t, resolve_int(ttarget))
                    s = state.gensym()
                    if ttarget.size() < 8:
                        sz = "w"
                    else:
                        sz = "l"
                    yield from emit_ext(op, v, s, sz)
                    state.push_t(s, ttarget)
                else:
                    check(
                        False,
                        f"Value of type {t} cannot be cast to {ttarget}.",
                        params[0].tk,
                    )

            else:
                d = len(sig["params"]) - len(params)
                if d > 0:
                    check(
                        False,
                        f"This procedure requires {d} more parameter(s).",
                        func.tk,
                    )
                elif d < 0:
                    check(
                        False,
                        f"This procedure has been provided {-d} unexpected parameter(s).",
                        func.tk,
                    )
                for p, t in zip(params, sig["params"]):
                    yield from compile_param(p, state)

                cparams = state.pop_t(len(params))

                fparams = []
                for p, (v, ct), rt in zip(params, cparams, sig["params"]):
                    check_type(rt, ct, p.tk)
                    if ct == AtomType("any_int"):
                        fparams.append((v, rt.resolve()))
                    else:
                        fparams.append((v, ct.resolve()))

                norm_name = normalize(sig["func_name"])

                if isniltype(sig["returns"]):
                    yield from emit_nil_call(norm_name, fparams)
                    state.push_t(Nil(func.tk), AtomType("nil"))
                else:
                    var = state.gensym()
                    if isinstance(sig["returns"].resolve(), RecordType):
                        yield from emit_call_outparam(
                            var, sig["returns"].size(), norm_name, fparams
                        )
                    else:
                        yield from emit_call(sig["returns"].resolve(), var, norm_name, fparams)
                    state.push_t(var, sig["returns"])
    elif cmd.trig == Kw.IMPORT:
        state.push_t(Nil(cmd.tk), AtomType("nil"))
    else:
        raise CFError(
            f"{cmd.trig} is not implmented yet or should not be found in analysed code.",
            cmd.tk,
        )
    assert (
        len(state.stack) == n + 1
    ), f"Command should add exactly one value to the stack {cmd}"


def compile_param(param, state):
    assert isinstance(state, CompileState)
    assert isinstance(param, Param)
    n = len(state.stack)
    if isinstance(param, Symbol):
        v, t = state.lookup_t(param)
        if isglobal(v) and not isinstance(t.resolve(), RecordType):
            s = state.gensym(name=param.name+"_v")
            yield from emit_load(t.resolve(), s, v)
            state.push_t(s, t)
        else:
            state.push_t(v, t)
    elif isinstance(param, Number):
        state.push_t(param, AtomType("any_int"))
    elif isinstance(param, String):
        s = state.gensym(SymType.GLOBAL)
        if param.cstr:
            state.register_data(s, param.value)
            state.push_t(s, AtomType("ptr"))
        else:
            rec = state.gensym()
            state.register_data(s, param.value)

            l = len(param.value) - 1

            s_p = rec
            s_l = state.gensym()
            yield from emit_alloc(rec, 16)
            yield from emit_binop("add", AtomType("int"), s_l, rec, 8)
            yield from emit_store(AtomType("int"), s_p, s)
            yield from emit_store(AtomType("int"), s_l, l)
            state.push_t(rec, StringRec)
    elif isinstance(param, Block):
        with state.new_scope():
            state.push_t(Nil(param.tk), AtomType("nil"))
            for cmd in param.cmds:
                state.pop()
                yield from compile_cmd(cmd, state)
    else:
        raise CFError(f"WTF {param}", param.tk)
    assert (
        len(state.stack) == n + 1
    ), "There should be exactly one more value on the stack."


def compile_type_expr(param, state):
    assert isinstance(state, CompileState)
    assert isinstance(param, (Param, Type))
    if isinstance(param, Type):
        return param
    elif isinstance(param, Symbol):
        return state.lookup_type(param)
    elif isinstance(param, Number):
        raise CFError("A number is not a valid type.", param.tk)
    elif isinstance(param, String):
        raise CFError("A string is not a valid type.", param.tk)
    elif isinstance(param, Block):
        check(
            len(param.cmds) == 1,
            "In type definition, blocks must contains exactly one command",
            param.tk,
        )
        return compile_type_cmd(param.cmds[0], state)
    else:
        raise CFError(f"WTF {param}", param.tk)


def compile_type_cmd(cmd, state):
    check(cmd.trig == Kw.CALL, "This is not a valid type form.", cmd.tk)
    func, *params = cmd.params
    assert isinstance(func, Symbol)

    if func.name == "record":
        fields = []
        for field, type_ in pairs(params):
            check(isinstance(field, Symbol), "a field can only be a symbol", field.tk)

            t = compile_type_expr(type_, state)
            fields.append((field.name, t))

        return RecordType(FrozenDict(fields))

    elif func.name == "distinct":
        check(len(params) == 1, "distinct takes exactly one parameter", func.tk)
        t = compile_type_expr(params[0], state)
        return DistinctType(t)

    elif func.name == "&":
        raise NotImplementedError("Not yet")

    else:
        raise CFError(f"{func.name} is not a valid record form.", func.tk)


def binary_reduce(emit_op, neutral, state, exprs):
    if len(exprs) == 0:
        state.push(neutral)
    elif len(exprs) == 1:
        state.push(exprs[0])
    else:
        n = len(exprs)
        e1 = exprs[: n // 2]
        e2 = exprs[n // 2 :]
        yield from binary_reduce(emit_op, neutral, state, e1)
        yield from binary_reduce(emit_op, neutral, state, e2)
        s = state.gensym()
        a, b = state.pop(2)
        yield from emit_op(s, a, b)
        state.push(s)


def emit_call(type_: Type, var: str, func: str, params: list[tuple[str, Type]]):
    assert type_.resolve() == type_
    yield f"    {var} ={ssa_first_class_type(type_)} call ${func}("
    yield ", ".join(f"{ssa_first_class_type(type_)} {p}" for p, type_ in params)
    yield ")\n"


def emit_call_outparam(var: str, size: int, func: str, params: list[tuple[str, Type]]):
    assert all(t.resolve() == t for _, t in params)
    yield from emit_alloc(var, size)
    yield f"    call ${func}("
    yield f"l {var}, "
    yield ", ".join(f"{ssa_first_class_type(pt)} {p}" for p, pt in params)
    yield ")\n"


def emit_nil_call(func: str, params: list[tuple[str, Type]]):
    assert all(t.resolve() == t for _, t in params)
    yield f"    call ${func}("
    yield ", ".join(f"{ssa_first_class_type(type_)} {p}" for p, type_ in params)
    yield ")\n"


def emit_bind(type_: Type, var: str | int, val: str | int):
    assert type_.resolve() == type_
    bind_t = "w" if type_.size() < 8 else "l"
    yield f"    {var} ={bind_t} copy {val}\n"


def emit_cmp(type_: Type, cmps: tuple[str, str, str], var: str, a: str | int, b: str | int):
    assert type_.resolve() == type_
    assert isinttype(type_) and isinstance(type_, AtomType)
    signed, unsigned, float_ = cmps
    sign, sz = int_types[type_.value]

    assert sign in {"s", "u"}
    op = signed if sign == "s" else unsigned

    sz_cmp = "l" if sz == "l" else "w"

    yield f"    {var} ={sz_cmp} c{op}{sz_cmp} {a}, {b}\n"


def emit_binop(op: str, type_: Type, var: str, a: str | int, b: str | int):
    # TODO handle the type w
    yield f"    {var} ={ssa_first_class_type(type_)} {op} {a}, {b}\n"


def emit_unop(op: str, var: str, a: str | int):
    # TODO handle the type w
    yield f"    {var} =l {op} {a}\n"


def emit_jmp(state: CompileState, label: str):
    state.running_block = False
    yield f"    jmp {label}\n"


def emit_label(state: CompileState, label: str):
    state.running_block = True
    yield f"{label}\n"


def emit_test(state: CompileState, truelbl: str, falselbl: str, test: str | int):
    state.running_block = False
    yield f"    jnz {test}, {truelbl}, {falselbl}\n"


def emit_return(state: CompileState, val=None):
    state.running_block = False
    if val is not None and not isinstance(val, Nil):
        yield f"    ret {val}\n"
    else:
        yield "    ret\n"


def emit_alloc(var, size):
    yield f"    {var} =l alloc8 {size}\n"


def emit_store(type_, target, value):
    assert type_.resolve() == type_
    store_type = ssa_type(type_)
    yield f"    store{store_type} {value}, {target}\n"


def emit_load(type_: Type, var: str, target: str):
    assert type_.resolve() == type_
    if isinttype(type_):
        assert isinstance(type_, AtomType)
        sign, size = int_types[type_.value]
        if size == "l":
            bind_type = "l"
            load_type = "l"
        else:
            bind_type = "w"
            load_type = sign + size
    else:
        bind_type = "l"
        load_type = "l"
    yield f"    {var} ={bind_type} load{load_type} {target}\n"


def emit_ext(op, src, target, size):
    yield f"    {target} ={size} ext{op} {src}\n"


def check_type(ref: Optional[Type], real: Type, tk: Token):
    assert ref is None or istype(ref)
    assert istype(real), f"{ref = } {real = }"
    if isinstance(ref, str):
        ref = AtomType(ref)

    if isinstance(real, str):
        real = AtomType(real)

    check(
        ref is None or ref == real
        or (real == AtomType("any_int") and isinttype(ref))
        or (ref == AtomType("any_int") and isinttype(real)),
        f"Incompatible types: expected {ref} but got {real}.",
        tk,
    )


def unzip0(seq):
    return [a for a, *b in seq]


def eval_const(param, state):
    assert isinstance(state, CompileState)
    assert isinstance(param, Param)
    if isinstance(param, Symbol):
        glob = "$" + normalize(param.name)
        if isinstance(state.data.get(glob, None), Constant):
            return state.data[glob]
        else:
            raise CFError(f"{param.name} is not a compile time value.", param.tk)

    elif isinstance(param, Number):
        return Constant(AtomType("any_int"), param.num)

    elif isinstance(param, String):
        if String.cstr:
            return Constant(StringRec, param.value)
        else:
            return Constant(AtomType("ptr"), param.value.encode("utf8"))

    else:
        raise CFError(f"{param} is not currently supported in constant definitions", param.tk)


class Constant:
    def __init__(self, type_, value):
        self.type_ = type_
        self.value = value


def isglobal(name):
    assert isinstance(name, str)
    return name.startswith("$")


def reduce_int_types(params, exprs):
    t_tot = AtomType("any_int")

    for p, (_, t) in zip(params, exprs):
        check(isinttype(t), f"Incompatible types: expected some int but got {t}.", p.tk)
        if t != AtomType("any_int"):
            if t_tot != AtomType("any_int"):
                check_type(t_tot, t, p.tk)
            else:
                t_tot = t

    if t_tot == AtomType("any_int"):
        t_tot = AtomType("int")

    return t_tot
