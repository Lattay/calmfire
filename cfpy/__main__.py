""" A toy native compiler using QBE.
"""

from os import unlink
from os.path import join
from subprocess import run
from argparse import ArgumentParser
from enum import Enum

from .compiler import src_to_ssa
from .misc import std_dir


DEBUG = True
quite = False

use_diet = False

no_libc_rt = join(std_dir, "no_libc/rt.s")
libc_rt = join(std_dir, "c/rt.s")


class Libc(Enum):
    DIET = 0
    MUSL = 1
    GNU = 2


def main():
    global quite

    args = parse_args()
    quite = args.quite
    src = args.input
    rad = ".".join(src.split(".")[:-1])
    ssa = rad + ".ssa"
    target = rad
    ok = src_to_ssa(src, ssa, test_mode=args.test_mode)
    if not ok:
        exit(1)
    ssa_to_exec(ssa, target, keep_s=args.asm, keep_ssa=args.ssa, no_libc=args.no_libc)

    if args.run:
        cmd = ("" if target.startswith("/") else "./") + target
        exit(call_and_log(cmd))
    else:
        exit(0)


def ssa_to_exec(src, target, keep_s=False, keep_ssa=False, no_libc=True):
    opts = ""
    asm_target = target + ".s"

    ret = call_and_log(f"qbe {opts} {src} -o {asm_target}")

    if not keep_ssa and ret == 0:
        unlink(src)
    if ret != 0:
        exit(1)

    opts = ""

    if DEBUG:
        opts += "-g"


    if no_libc:
        ret = call_and_log(f"as {no_libc_rt} {opts} {asm_target} -o interm.o")
    else:
        ret = call_and_log(f"as {libc_rt} {opts} {asm_target} -o interm.o")

    if not keep_s and ret == 0:
        unlink(asm_target)

    if ret != 0:
        exit(1)

    if no_libc:
        call_and_log(f'ld -static interm.o -o "{target}"')
    elif use_diet:
        call_and_log(f'ld -static -o "{target}" -L/opt/diet/lib-x86_64 '
                     "/opt/diet/lib-x86_64/start.o interm.o "
                     "/opt/diet/lib-x86_64/libc.a")

    else:
        call_and_log(f'ld --sysroot=/usr/lib/musl -static -o "{target}" '
                     "-L/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0 "
                     "-L/usr/lib/musl/lib/ "
                     "-l:crt1.o -l:crti.o "
                     "-l:crtbeginT.o "
                     "-L-user-start -L-user-end "
                     "interm.o --start-group -lc "
                     "--end-group -l:crtend.o "
                     "-l:crtn.o")
    unlink("interm.o")


def parse_args():
    parser = ArgumentParser(description=__doc__.splitlines()[0])

    parser.add_argument(
        "--run",
        "-r",
        action="store_true",
        help="Execute the program right away.",
    )
    parser.add_argument(
        "--ssa",
        "-q",
        action="store_true",
        help="Leave the QBE .ssa file",
    )
    parser.add_argument(
        "--asm",
        "-s",
        action="store_true",
        help="Leave the GAS .s file",
    )
    parser.add_argument(
        "--quite",
        action="store_true",
        help="Do not log the commands",
    )
    parser.add_argument(
        "--test-mode",
        action="store_true",
        help="Use test mode (add some builtins)",
    )
    parser.add_argument(
        "--no-libc",
        action="store_const",
        const=True,
        default=False,
        help="Do no link against libc",
    )
    parser.add_argument(
        "--use-glibc",
        action="store_const",
        const=Libc.GNU,
        default=Libc.MUSL,
        help="Link against glibc",
    )
    parser.add_argument(
        "--use-dietlibc",
        action="store_const",
        const=Libc.DIET,
        help="Link against dietlibc",
    )
    parser.add_argument(
        "--use-musl",
        action="store_const",
        const=Libc.MUSL,
        help="Link against musl",
    )

    parser.add_argument("input", help="Input file.")

    return parser.parse_args()


def call_and_log(cmd):
    if not quite:
        print(f"$ {cmd}")
    return run(cmd, shell=True).returncode

if __name__ == "__main__":
    main()
