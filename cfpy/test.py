import os.path
from enum import Enum
from subprocess import run
import json
from difflib import unified_diff

from pysh import find, cwd

from .misc import install_dir


def tests(overwrite=False):
    for file in find("tests/no_libc_no_std/").name(r".*\.cf"):
        path = file.relative_to(cwd()).to_str()
        res, msg = test_file(path, write=overwrite, no_std=True, no_libc=True)
        test_res(path, res, msg)

    for file in find("tests/no_libc/").name(r".*\.cf"):
        path = file.relative_to(cwd()).to_str()
        res, msg = test_file(path, write=overwrite, no_libc=True)
        test_res(path, res, msg)

    for file in find("tests/libc_std/").name(r".*\.cf"):
        path = file.relative_to(cwd()).to_str()
        res, msg = test_file(path, write=overwrite)
        test_res(path, res, msg)


def test_res(path, res, msg):
    pad = "." * (70 - len(path))
    if res == Result.SUCCESS:
        print(f"{path} {pad} OK")
    elif res == Result.FAILED:
        print(f"{path} {pad} FAILED")
        print(msg)
    elif res == Result.EMPTY:
        print(f"{path} {pad} EMPTY")
        print(msg)
    elif res == Result.OVERWRITTEN:
        print(f"{path} {pad} OVERWRITTEN")


def test_file(file, write=False, no_std=False, no_libc=False):
    opts = ""

    if no_std:
        opts += "--test-mode"

    if no_libc:
        opts += " --no-libc"

    proc = run(
        f"python -m cfpy {opts} --quite -r {file}",
        shell=True,
        text=True,
        capture_output=True,
    )

    out = proc.stdout
    err = proc.stderr
    ret = proc.returncode

    refs = get_test_refs(file)
    new_refs = {
        "out": out,
        "err": err,
        "ret": ret,
    }

    if refs is None:
        if write:
            set_test_refs(file, new_refs)
            return Result.OVERWRITTEN, ""
        else:
            msg = (
                "# STDOUT\n"
                + out
                + "# STDERR\n"
                + err
                + "# RETCODE\n"
                + str(ret)
            )
            return Result.EMPTY, msg

    elif refs["err"] != err:
        res = unified_diff(
            refs["err"].splitlines(keepends=True),
            err.splitlines(keepends=True),
            fromfile="reference",
            tofile="stderr",
        )
        if write:
            set_test_refs(file, new_refs)
        return Result.FAILED, "".join(res)

    elif refs["out"] != out:
        res = unified_diff(
            refs["out"].splitlines(keepends=True),
            out.splitlines(keepends=True),
            fromfile="reference",
            tofile="stdout",
        )
        if write:
            set_test_refs(file, new_refs)
        return Result.FAILED, "".join(res)

    elif refs["ret"] != ret:
        if write:
            set_test_refs(file, refs)
        return Result.FAILED, (f"Expected retcode {refs['ret']} but got {ret}.")

    return Result.SUCCESS, ""


class Result(Enum):
    SUCCESS = 0
    FAILED = 1
    EMPTY = 2
    OVERWRITTEN = 3


def get_test_refs(file):
    try:
        with open(file + ".json", "r") as f:
            return json.load(f)
    except FileNotFoundError:
        return None


def set_test_refs(file, refs):
    with open(file + ".json", "w") as f:
        json.dump(refs, f)


if __name__ == "__main__":
    import sys

    tests(len(sys.argv) > 1 and sys.argv[1] == "--overwrite")
