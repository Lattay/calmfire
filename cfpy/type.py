from dataclasses import dataclass
from abc import abstractmethod
from .misc import CFError, check, pairs


from .parser import Symbol

from .misc import FrozenDict


class Type:
    def resolve(self):
        return self

    @abstractmethod
    def size(self):
        ...


@dataclass(frozen=True)
class AtomType(Type):
    value: str

    def __str__(self):
        return self.value

    def size(self):
        assert self.value in builtins_types
        return builtins_types[self.value]

    def alignment(self):
        return self.size()


@dataclass(frozen=True)
class DistinctType(Type):
    model: Type

    def __str__(self):
        return "distinct/" + str(self.model)

    def size(self):
        return self.model.size()

    def alignment(self):
        return self.model.alignment()

    def resolve(self):
        return self.model.resolve()


@dataclass(frozen=True)
class RecordType(Type):
    fields: FrozenDict

    def size(self):
        s = 0
        al = self.alignment()
        for f, t in self.fields.items():
            s += s % t.alignment()  # padding
            s += t.size()
        s += s % al
        return s

    def get_offset(self, field_name):
        s = 0
        for f, t in self.fields.items():
            s += s % t.alignment()  # padding
            if f == field_name:
                break
            else:
                s += t.size()
        return s

    def alignment(self):
        return max(f.alignment() for f in self.fields.values())

    def __str__(self):
        return "record { " + " ".join(f"{f} {t}" for f, t in self.fields.items()) + " }"


def istype(thing):
    return thing in builtins_types or isinstance(thing, Type)


def isniltype(type):
    assert isinstance(type, (Symbol, Type))
    if isinstance(type, Symbol):
        return type.name == "nil"
    else:
        return isinstance(type, AtomType) and type.value == "nil"


builtins_types = {
    "int": 8,
    "uint": 8,
    "i32": 4,
    "u32": 4,
    "i16": 2,
    "u16": 2,
    "i8": 1,
    "u8": 1,
    "ptr": 8,
    "nil": 0,
}


def resolve_int(t1, t2=None):
    assert isinttype(t1) and (t2 is None or isinttype(t2))
    if t1 != AtomType("any_int"):
        return t1 or AtomType("int")
    else:
        if t2 != AtomType("any_int"):
            return t2 or AtomType("int")
        else:
            return AtomType("int")


def ssa_type(t: Type):
    assert isinstance(t, Type)
    if isinttype(t):
        assert isinstance(t, AtomType)
        _, ssa_t = int_types[resolve_int(t).value]
        return ssa_t
    elif isniltype(t):
        return ""
    else:
        return "l"


def ssa_first_class_type(t: Type):
    assert isinstance(t, Type), f"{t}"
    if t.size() < 8:
        return "w"
    else:
        return "l"


def check_cast_compat(src, target, tk):
    check(
        free_cast(src, target),
        f"Value of type {src} cannot be cast to {target}.",
        tk,
    )


def free_cast(src, target):
    assert isinstance(src, Type)
    assert isinstance(target, Type)
    return (
        (isinstance(src, RecordType) and target == AtomType("ptr"))
        or (isinstance(target, RecordType) and src == AtomType("ptr"))
        or (src == AtomType("int") and target == AtomType("ptr"))
        or (src == AtomType("uint") and target == AtomType("ptr"))
        or (target == AtomType("int") and src == AtomType("ptr"))
        or (target == AtomType("uint") and src == AtomType("ptr"))
        or (src == AtomType("any_int") and isinttype(target.resolve()))
        or (src.resolve() == target.resolve())
        # TODO check that this is not less strict than what QBE actually allow
        or (isinttype(src) and isinttype(target) and src.size() >= target.size())
    )


def op_cast(src, target):
    return not free_cast(src, target) and isinttype(src) and isinttype(target)


def isinttype(t: Type):
    assert isinstance(t, Type)
    return isinstance(t, AtomType) and (t.value in int_types or t.value == "any_int")


def convert(src, target):
    assert op_cast(src, target)
    nsrc = src.value
    sign_s, len_s = int_types[nsrc]
    return sign_s + len_s


int_types = {
    "int": ("s", "l"),
    "uint": ("u", "l"),
    "i32": ("s", "w"),
    "u32": ("u", "w"),
    "i16": ("s", "h"),
    "u16": ("u", "h"),
    "i8": ("s", "b"),
    "u8": ("u", "b"),
}


String = RecordType(FrozenDict({"raw": AtomType("ptr"), "len": AtomType("uint")}))
