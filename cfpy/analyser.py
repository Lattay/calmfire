from .misc import CFError, check, pairs, normalize
from .parser import Kw, Block, Command, Symbol
from .type import Type


def seam_ifs(cmds):
    prev = None

    for cmd in cmds:
        if cmd.trig == Kw.IF:
            if prev:
                yield prev
            check(len(cmd.params) == 2, "if form takes exactly two params.", cmd.tk)
            prev = Command(Kw.IF, list(map(seam_ifs_param, cmd.params)), cmd.tk)
        elif cmd.trig == Kw.ELSE:
            check(len(cmd.params) == 1, "else form takes exactly one param.", cmd.tk)
            if not prev:
                raise CFError("orphan else", cmd.tk)
            else:
                body = seam_ifs_param(cmd.params[0])
                prev.params.append(body)
                yield prev
                prev = None
        elif cmd.trig == Kw.ELIF:
            check(len(cmd.params) == 2, "elif form takes exactly two params.", cmd.tk)
            if not prev:
                raise CFError("orphan elif", cmd.tk)
            else:
                cond = seam_ifs_param(cmd.params[0])
                body = seam_ifs_param(cmd.params[1])
                prev.params.append(cond)
                prev.params.append(body)
        else:
            if prev:
                yield prev
                prev = None
            yield Command(cmd.trig, list(map(seam_ifs_param, cmd.params)), cmd.tk)
    if prev:
        yield prev


def seam_ifs_param(param):
    if isinstance(param, Block):
        return param.__class__(param.tk, list(seam_ifs(param.cmds)))
    return param


def topbottom(cmds):
    for cmd in cmds:
        topbottom_cmd(cmd, toplevel=True)


def topbottom_cmd(cmd, toplevel=False):
    if not toplevel:
        if cmd.trig == Kw.PROC:
            raise CFError("Procedure can only be defined at toplevel.", cmd.tk)
        if cmd.trig == Kw.TYPE:
            raise CFError("Type can only be defined at toplevel.", cmd.tk)
        if cmd.trig == Kw.IMPORT:
            raise CFError("import can only be used at top level.", cmd.tk)
        if cmd.trig == Kw.EXPORT:
            raise CFError("export can only be used at top level.", cmd.tk)
        if cmd.trig == Kw.GLOB:
            raise CFError("glob can only be used at top level.", cmd.tk)
        if cmd.trig == Kw.DECL:
            raise CFError("decl can only be used at top level.", cmd.tk)
    else:
        if cmd.trig not in {Kw.PROC, Kw.IMPORT, Kw.EXPORT, Kw.TYPE, Kw.GLOB, Kw.DECL}:
            raise CFError("this is not a valid top level form.", cmd.tk)

    for p in cmd.params:
        topbottom_param(p)


def topbottom_param(p):
    if isinstance(p, Block):
        for cmd in p.cmds:
            topbottom_cmd(cmd)


def collect_proc_sig(reg, cmds, istype):
    for cmd in cmds:
        if cmd.trig == Kw.PROC:
            check(
                len(cmd.params) >= 3,
                "Procedure must have at least a name, a return type and a body.",
                cmd.tk,
            )
            sym, retype, *params = cmd.params[:-1]

            check(isinstance(sym, Symbol), "Only symbols can identify procedures.", sym.tk)

            if len(params) % 2 != 0:
                raise CFError("There is an untyped parameter or an unexpected type.", params[0].tk)
            typed_params = list(pairs(params))

            params = []

            for param, type in typed_params:
                params.append(type)

            register_proc(reg, sym, retype, params)

        elif cmd.trig == Kw.DECL:
            check(
                len(cmd.params) >= 2,
                "Procedure declaration must have at least a name and a return type.",
                cmd.tk,
            )
            sym, retype, *params = cmd.params

            check(isinstance(sym, Symbol), "Only symbols can identify procedures.", sym.tk)

            if len(params) % 2 != 0:
                raise CFError("There is an untyped parameter or an unexpected type.", params[0].tk)
            typed_params = list(pairs(params))

            params = []

            for param, type in typed_params:
                params.append(type)

            register_proc(reg, sym, retype, params)


def collect_exported(exp, cmds):
    for cmd in cmds:
        if cmd.trig == Kw.EXPORT:
            check(
                len(cmd.params) == 1,
                "export takes exactly one parameter",
                cmd.tk
            )
            check(
                isinstance(cmd.params[0], Symbol),
                "only symbols can be exported",
                cmd.tk,
            )
            exp.add(normalize(cmd.params[0].name))


def collect_types(reg, cmds, istype, compile_type):
    for cmd in cmds:
        if cmd.trig == Kw.TYPE:
            check(len(cmd.params) == 2, "type takes two parameters.", cmd.tk)
            sym, type_ = cmd.params
            check(isinstance(sym, Symbol), f"Cannot bind a type to {sym}.", cmd.tk)
            v = compile_type(type_)
            register_type(reg, sym, v)


def register_proc(reg, sym: Symbol, retype, params):
    if sym.name in reg:
        raise CFError(f"A procedure {sym.name} already exists.", sym.tk)
    reg[sym.name] = {
        "returns": retype,
        "params": params,
    }


def register_type(reg, sym, type_):
    assert isinstance(sym, Symbol), f"Unexpected type name {sym}"
    assert isinstance(type_, Type), f"Unexpected type value {type_}"

    check(
        sym.name not in reg,
        f"{sym.name} is already bound to a type.",
        sym.name,
    )

    reg[sym.name] = type_
