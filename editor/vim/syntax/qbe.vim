scriptencoding utf-8
" Vim syntax file
" Language: QBE IL
" Maintainer: Théo Cavignac
" Latest Revision: 2022-02-15

if exists('b:current_syntax')
  finish
endif

syn iskeyword @,36-37,48-58,64-90,97-122


syn keyword qbeKeyword function export data type
syn keyword qbeInstruct add and div mul or rem sar shl shr sub udiv urem xor
syn keyword qbeInstruct alloc16 alloc4 alloc8 loadd loadl loads loadsb loadsh
syn keyword qbeInstruct loadsw loadub loaduh loaduw loadw storeb stored storeh
syn keyword qbeInstruct storel stores storew ceqd ceql ceqs ceqw cged cges cgtd
syn keyword qbeInstruct cgts cled cles cltd clts cned cnel cnes cnew cod cos
syn keyword qbeInstruct csgel csgew csgtl csgtw cslel cslew csltl csltw cugel
syn keyword qbeInstruct cugew cugtl cugtw culel culew cultl cultw cuod cuos
syn keyword qbeInstruct dtosi dtoui exts extsb extsh extsw extub extuh extuw
syn keyword qbeInstruct sltof ultof stosi stoui swtof uwtof truncd cast copy
syn keyword qbeInstruct call vastart vaarg phi jmp jnz ret
syn keyword qbeType l w h b d s

syn match qbeNumber '[0-9]\+'
syn match qbeLocal '%\k\+'
syn match qbeGlobal '$\k\+'
syn match qbeLabel '@\k\+'
syn match qbeAggregate ':\k\+'
syn region qbeString start='"' end='"'
syn region qbeBlock start="{" end="}" fold transparent
let b:current_syntax = 'qbe'

hi def link qbeKeyword Keyword
hi def link qbeNumber Constant
hi def link qbeString String
hi def link qbeLocal Normal
hi def link qbeGlobal Normal
hi def link qbeLabel Normal
hi def link qbeInstruct Function
hi def link qbeType Type
hi def link qbeAggregate Type
