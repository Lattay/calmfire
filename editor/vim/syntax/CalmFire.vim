scriptencoding utf-8
" Vim syntax file
" Language: CalmFire
" Maintainer: Théo Cavignac
" Latest Revision: 2022-02-15

if exists('b:current_syntax')
  finish
endif

syn iskeyword @,33,35-39,42-57,60-122,124,126,192-255

syn keyword cfKeyword if else elif while let set proc decl return import export type glob
syn keyword cfBuiltin set!1 + - * / &+ &- mod < > >= <= = != 0? 1? -! +! &+! &-! &local record distinct extern
syn keyword cfType int uint i32 u32 i16 u16 i8 u8 ptr nil

syn match cfSymbol '\k\+'
syn match cfNumber '-\=[0-9]\+'
syn region cfString start='"' end='"'
syn region cfCString start='c"' end='"'
syn region cfParen start="(" end=")" fold transparent
syn region cfBlock start="{" end="}" fold transparent
syn match cfColon ":"
syn match cfSemicolon ";"

syn match cfComment '//.*' contains=@Spell
let b:current_syntax = 'CalmFire'

hi def link cfKeyword Keyword
hi def link cfColon Keyword
hi def link cfComment Comment
hi def link cfNumber Constant
hi def link cfString String
hi def link cfCString String
hi def link cfSymbol Normal
hi def link cfBuiltin Function
hi def link cfType Type
