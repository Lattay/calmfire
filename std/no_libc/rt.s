.text

.globl syscall1
syscall1:
        movq %rdi, %rax
        movq %rsi, %rdi
        syscall
        ret

.globl syscall2
syscall2:
        movq %rdi, %rax
        movq %rsi, %rdi
        movq %rdx, %rsi
        movq %rcx, %rdx
        syscall
        ret

.globl syscall3
syscall3:
        movq %rdi, %rax
        movq %rsi, %rdi
        movq %rdx, %rsi
        movq %rcx, %rdx
        syscall
        ret

.globl syscall4
syscall4:
        movq %rdi, %rax
        movq %rsi, %rdi
        movq %rdx, %rsi
        movq %rcx, %rdx
        movq %r8, %r10
        syscall
        ret

.globl exit
exit:
	pushq %rbp
	movq %rsp, %rbp
	movq %rdi, %rsi
	movl $60, %edi
	callq syscall1
	leave
	ret
/* end function exit */

.text
.globl	cf_memcpy
cf_memcpy:
.LFB0:
	testq	%rdx, %rdx
	je	.L1
	xorl	%eax, %eax
.L3:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L3
.L1:
	ret

.text
.globl _start
_start:
	pushq %rbp
	movq %rsp, %rbp
	callq main
	movq %rax, %rdi
	callq exit
	movl $0, %eax
	leave
	ret
/* end function _start */
