.text
.globl	cf_memcpy
cf_memcpy:
.LFB0:
	testq	%rdx, %rdx
	je	.L1
	xorl	%eax, %eax
.L3:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L3
.L1:
	ret
