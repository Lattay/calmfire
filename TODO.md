- [X] Unsigned integers
- [X] Make an executable
- [X] Arithmetic
- [X] Conditionals
- [X] Loops
- [X] Procedures
- [X] Pointers
- [X] Type system
- [X] C Strings
- [X] Structs
    - [X] Allocating on the stack
    - [X] Accessing fields
    - [X] Returning
- [X] Includes
- [X] Strings
- [X] Signed and unsigned integers
- [X] Remove type assignation on number literals
- [ ] Arrays
- [X] Opt-out linking against libc
- [ ] constants globals
- [X] mutable globals
- [ ] Bootstrap
- [ ] Macros
- [.] Advanced structs and other compound types
    - [ ] Copying
    - [X] Opaque alias type (distinct)
    - [ ] Create pointers to
    - [ ] Accessing nested fields
    - [ ] zero initializing
    - [ ] defining globally
    - [ ] union
    - [ ] enum
    - [ ] function pointers
    - [ ] passing struct by value for C interop
- [ ] Modules (makes includes obselete)
- [ ] Floating point numbers
- [.] Standard library
    - [ ] libc/nolibc independant
        - [ ] assert
        - [ ] test
        - [ ] build
        - [ ] generic algorithms and data structures
        - [ ] useful macros
    - [ ] with libc
        - [ ] basic bindings
        - [ ] libm bindings
    - [.] without libc
        - [.] File IO without libc
            - [X] Basic open, write, close
            - [ ] Basic open, read, close
            - [ ] Buffered IO
            - [ ] Advanced workflow (append, truncate, chose permissions...)
