""" Script to help convert C headers into CalmFire stubs.

Works with pretty clean headers.
Limitations:
- no handling of macros
- not aware of type specifiers (const, extern...)
- ignore anything after the parameter list
- unable to handle func pointer (but CF is too anyway)

Really it is not smart, it just make the boring work a bit easier.
"""

import sys


def convert(source):
    for line in source:
        stripped_l = line.strip()
        if stripped_l.startswith("#"):
            print("// " + stripped_l)
        elif not stripped_l or stripped_l.startswith("//"):
            print(line, end="")
        else:
            try:
                pref, *rest = line.split("(")
                if len(rest) > 1:
                    assert ")" in rest[0], "Weird type with paren"
                    rest = [rest[0]]
                args, *_ = rest[0].split(")")
                fname, ret = make_type(pref)
                sig = [f"decl {fname} {ret}"]
                if args != "void":
                    for arg in args.split(","):
                        n, t = make_type(arg)
                        sig.append(n)
                        sig.append(t)
                print(" ".join(sig) + ";")
            except Exception as e:
                print(f"// Don't no what to do with this line ({e}):")
                print(line, end="")


def make_type(pref):
    name, *rettype = reversed(pref.strip().split())
    assert len(rettype) > 0, "Weird type"
    if name.startswith("*"):
        name = name.replace("*", "")
        t = to_pascal_case(reversed(rettype)) + "*"
    else:
        t = to_pascal_case(reversed(rettype))

    if t.endswith("Int"):
        t = "i32"
    elif t.endswith("LongInt") or t.endswith("Size_t"):
        t = "int"
    elif t.endswith("Void"):
        t = "nil"

    return name, t


def to_pascal_case(symbols):
    return "".join(s[0].upper() + s[1:] for s in symbols)


if __name__ == "__main__":
    convert(sys.stdin.readlines())
